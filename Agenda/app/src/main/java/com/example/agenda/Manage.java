package com.example.agenda;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class Manage extends AppCompatActivity {

    EditText editTextName;
    EditText editTextPhone;
    EditText editTextAddress;
    Button buttonConfirm;

    private int index=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        setTitle("List Agenda");
        editTextName=findViewById(R.id.editTextName);
        editTextPhone=findViewById(R.id.editTextPhone);
        editTextAddress=findViewById(R.id.editTextAddress);
        buttonConfirm=findViewById(R.id.buttonConfirm);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            index = extras.getInt("edit");
            editTextName.setText(DataModel.getInstance().getCurrentPerson().getName());
            editTextPhone.setText(DataModel.getInstance().getCurrentPerson().getPhone());
            editTextAddress.setText(DataModel.getInstance().getCurrentPerson().getAddress());
        }


    }

    public void onClickButtonConfirm(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(
                Manage.this);
        builder.setTitle(android.R.string.dialog_alert_title);
        if(index == -1) {
            builder.setMessage("Do you want to add this person?");
        }
        else{
            builder.setMessage("Do you want to edit this person?");
        }
        builder.setNegativeButton(android.R.string.no,null);
        builder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String name=editTextName.getText().toString();
                        String phone=editTextPhone.getText().toString();
                        String address=editTextAddress.getText().toString();
                        if(index == -1) {
                            DataModel.getInstance().listPeople.add(new Person(name, phone, address));
                        }
                        else{
                            DataModel.getInstance().getCurrentPerson().setName(name);
                            DataModel.getInstance().getCurrentPerson().setPhone(phone);
                            DataModel.getInstance().getCurrentPerson().setAddress(address);
                        }
                        Intent intent = new Intent(Manage.this, List.class);
                        startActivity(intent);
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}